import uuid

from flask import Flask, request
from flask_restx import Api, Resource, fields

app = Flask(__name__)
tables = {}

@app.route('/')
def hello():
    return 'Hello, World!'

api = Api(app, doc="/docs", contact="quincy.hsieh[at]milliman.com", version="00.00.demo", title='RESTful API Example',
 description='The RESTful API Example on Tech Forum 2022',)

apiv1 = api.namespace('v1', description='RESTful API Version 1.0')

@apiv1.route('/table/new')
@apiv1.doc(security="access-token")
class NewTableResource(Resource):
    @apiv1.response(200, 'A string ID for a newly created table')
    def get(self):
        unique_id = str(uuid.uuid4())[:13]
        tables[unique_id] = {}
        return unique_id

model_configuration = apiv1.model('Model-configuration', {"model":fields.String, "shift":fields.Float(default=0.45), "data":fields.String})

@apiv1.route('/table/<string:model>')
@apiv1.doc(security="access-token", params={
    'table': { 'description': 'The table ID by endpoint GET /v1.0/table.', 'type': 'string'},
    'shift': { 'description': 'The shift parameter for LMM+ model', 'type':'string', 'default': 0.45} })
class TableModelResource(Resource):
    @apiv1.response(200, 'Success', model_configuration)
    @apiv1.doc(responses={
        406: 'Missing table ID'
    })
    def put(self, model):
        table_id = request.args.get('table', None)
        shift_param = request.args.get('shift', 0.45)
        if table_id is not None:
            tables[table_id] = {"model":model, "shift": float(shift_param)}
            return tables[table_id]
        else:
            return "Missing table ID", 406


@apiv1.route('/table/<string:model>/data')
@apiv1.doc(security="access-token", params={
    'table': { 'description': 'The table ID by endpoint GET /v1.0/table.', 'type': 'string'},
    'file': { 'description': 'The market data file for LMM+ model', 'type':'file'} })
class TableModelDataResource(Resource):
    @apiv1.response(200, 'Success', model_configuration)
    @apiv1.doc(responses={
        404: 'Missing data file',
        406: 'Missing table ID'
    })
    def post(self, model):
        table_id = request.form.get('table', None)
        data_file = sensi_config_file = request.files['file']
        if table_id is not None:
            if tables.get(table_id, None) is not None:
                if data_file != '':
                    try:
                        data_file_io = data_file.read()
                        data_file_text = data_file_io.decode('utf-8')
                    except Exception as e:
                        return f"Unable to read data file: {e}", 406

                    existing_table = tables[table_id]
                    existing_table["data"] = data_file_text
                    return existing_table
                else:
                    return "Missing data file", 406
            else:
                return f"{table_id} not found", 404
        else:
            return "Missing table ID", 406

    @apiv1.response(200, 'Success', model_configuration)
    @apiv1.doc(responses={
        404: '<Table_ID> not found',
        406: 'Missing table ID'
    })
    def delete(self, model):
        table_id = request.form.get('table', None)
        if table_id is not None:
            if tables.get(table_id, None) is not None:
                existing_table = tables[table_id]
                if "data" in existing_table:
                    existing_table.pop("data")
                return existing_table 
            else:
                return f"{table_id} not found", 404
        else:
            return "Missing table ID", 406


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3000, ssl_context='adhoc')
